using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    [Range(0f, 10f)]
    [SerializeField] private int speed;
    private float movement;
    public bool canAttack = true;
    public bool onground;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        //RaycastHit2D[] clls = Physics2D.RaycastAll(transform.position, Vector2.down).Where(item => item);
        //onground = 
        rb.velocity = new Vector2(movement, rb.velocity.y);
    }
    public void Movement(InputAction.CallbackContext ctx)
    {
        movement = ctx.ReadValue<float>() * speed;
    }
    public void Attack(InputAction.CallbackContext ctx)
    {
        if (canAttack)
        {
            RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position, 3f).Where(item => item.collider.name != "Player").ToArray();


            if (hits.Length > 0)
            {
                float angle = Vector2.Angle(Vector2.left, new Vector2(transform.position.x, transform.position.y) - hits[0].point) * Mathf.Deg2Rad;

                if (transform.position.y > hits[0].point.y) angle *= -1;

                //rb.velocity += new Vector2(20 * Mathf.Cos(angle), 20 * Mathf.Sin(angle)) * -( -hits[0].distance / 3 + 1);

                rb.AddForce(new Vector2(20 * Mathf.Cos(angle), 30 * Mathf.Sin(angle)) * -(-hits[0].distance / 3 + 1), ForceMode2D.Impulse);

                Debug.Log($"{angle * Mathf.Rad2Deg}�: {Mathf.Cos(angle)}, {Mathf.Sin(angle)}");
                Debug.DrawLine(transform.position, hits[0].point, Color.red, 0.5f);
                
                StartCoroutine(AttackCd());
            }
        }
    }
    IEnumerator AttackCd()
    {
        canAttack = false;
        yield return new WaitForSeconds(0.5f);
        canAttack = true;
    }
}
